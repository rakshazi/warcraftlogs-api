package warcraftlogs

import "fmt"

// Event - event info
type Event struct {
	Type        string `json:"type,omitempty"`
	Timestamp   int    `json:"timestamp,omitempty"`
	EncounterID int    `json:"encounterID,omitempty"`
	Name        string `json:"name,omitempty"`
	SourceID    int    `json:"sourceID,omitempty"`
	Gear        []Gear `json:"gear,omitempty"`
	Auras       []Aura `json:"auras,omitempty"`
}

// Gear info
type Gear struct {
	ID      int `json:"id,omitempty"`
	Quality int `json:"quality,omitempty"`
	Level   int `json:"itemLevel,omitempty"`
}

// Aura / buffs info
type Aura struct {
	SourceID  int    `json:"source,omitempty"`
	AbilityID int    `json:"ability,omitempty"`
	Stacks    int    `json:"stacks,omitempty"`
	Name      string `json:"name,omitempty"`
}

type eventsResponse struct {
	Events []*Event `json:"events,omitempty"`
}

// Events - todo
func (w *WarcraftLogs) Events(code string, view string, start int, end int) ([]*Event, error) {
	url := fmt.Sprintf("report/events/%s/%s?start=%v&end=%v", view, code, start, end)
	resp := &eventsResponse{}
	err := w.Get(url, resp)

	return resp.Events, err
}
